package ru.tsc.gavran.tm.api;

import ru.tsc.gavran.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
